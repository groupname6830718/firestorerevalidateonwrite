const functions = require('firebase-functions');
const admin = require('firebase-admin');
const axios = require('axios');
admin.initializeApp();

const fetchPlaceDocument = async (placeId) => {
  try {
    const placeDoc = await admin.firestore().collection('places').doc(placeId).get();
    return placeDoc.exists ? placeDoc.data() : null;
  } catch (error) {
    console.error(`Error getting place document with ID: ${placeId}, error: ${error}`);
    return null;
  }
};

const makeAxiosRequest = async (url) => {
  try {
    const response = await axios.get(url);
    console.log(response.data);
  } catch (error) {
    console.error(`Failed to make a GET request to URL: ${url}, error: ${error}`);
  }
};

exports.revalidatePaymentsOnWrite = functions.region("us-west2").firestore.document('payments/{docId}').onWrite(async (change) => {
  const document = change.after.exists ? change.after.data() : null;

  if (document && document.place) {
    const placeData = await fetchPlaceDocument(document.place);

    if (placeData && placeData.url) {
      const requestURL = `https://${placeData.url}/api/revalidate/payments`;
      await makeAxiosRequest(requestURL);
    }
   const requestURL = `https://nonprofit.name/api/revalidate/payments`;
    await makeAxiosRequest(requestURL);
  }
});


exports.revalidatePlaceDataOnWrite = functions.region("us-west2").firestore.document('places/{docId}').onWrite(async (change) => {
 const document = change.after.exists ? change.after.data() : null;

 if (document && document.url) {
  const requestURL = `https://${document.url}/api/revalidate/place-data`;
  await makeAxiosRequest(requestURL);
 }
 const requestURL = `https://nonprofit.name/api/revalidate/place-data`;
 await makeAxiosRequest(requestURL);
});
